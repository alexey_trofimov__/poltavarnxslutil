SETLOCAL EnableDelayedExpansion
cls

set UTIL_NAME=poltava-rn-xsl-util-2.0-SNAPSHOT-exec

rem Set path to Java 8
set JAVA8_HOME="C:/Program Files/Java/jdk1.8.0_311/"
rem Set profile name
set PROFILE=default

call %JAVA8_HOME%\bin\java.exe -jar -Dspring.profiles.active=%PROFILE% %UTIL_NAME%.jar 
rem > %UTIL_NAME%.log

ENDLOCAL