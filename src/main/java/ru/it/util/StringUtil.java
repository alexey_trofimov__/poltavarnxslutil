package ru.it.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtil {

    public String truncateOrPadStringToFixedLength(String str, int length, char padCharacter) {
        if (str.length() == length) {
            return str;
        }

        if (str.length() > length) {
            return str.substring(0, length);
        }

        return String.format("%1$-" + length + "s", str).replace(' ', padCharacter);
    }

    public String defineXsdTargetNamespace(String xsd) {
        String parts[] = xsd.split("targetNamespace\\s*=\\s*");

        if (parts.length > 1) {
            parts = parts[1].split("\\s+");

            if (parts.length > 0) { //убрать кавычки
                return parts[0].replaceAll("\\\"", "");
            }
        }

        return null;
    }
}
