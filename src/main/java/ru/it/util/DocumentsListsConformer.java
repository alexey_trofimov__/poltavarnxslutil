package ru.it.util;

import lombok.experimental.UtilityClass;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourceStatusEnum;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@UtilityClass
public class DocumentsListsConformer {

    public void conformLists(List<XmlResource> list1, List<XmlResource> list2) throws Exception {
        if (Objects.isNull(list1) || Objects.isNull(list2)) {
            throw new Exception("Null lists conformation attempt!");
        }

        // убираем заглушки
        list1.removeIf(r -> Objects.isNull(r.getId()));
        list2.removeIf(r -> Objects.isNull(r.getId()));

        /**
         * @see XmlResource#compareTo
         */
        Collections.sort(list1);
        Collections.sort(list2);

        int i1 = 0, i2 = 0;
        while (i1 < list1.size() && i2 < list2.size()) {
            XmlResource doc1 = list1.get(i1);
            XmlResource doc2 = list2.get(i2);

            int comparationResult = doc1.compareTo(doc2);

            if (comparationResult == 0) { //наименования совпали - сравниваем содержимое
                if (XmlResource.compareContents(doc1, doc2)) {
                    doc1.setStatus(XmlResourceStatusEnum.MATCHED);
                    doc2.setStatus(XmlResourceStatusEnum.MATCHED);
                } else {
                    doc1.setStatus(XmlResourceStatusEnum.UNMATCHED);
                    doc2.setStatus(XmlResourceStatusEnum.UNMATCHED);
                }

                i1++;
                i2++;
            }

            if (comparationResult > 0) {
                list1.add(i1, XmlResource.makeAbscentResource());

                doc2.setStatus(XmlResourceStatusEnum.MATCHED);

                i1++;
                i2++;
            }

            if (comparationResult < 0) {
                doc1.setStatus(XmlResourceStatusEnum.MATCHED);

                list2.add(i1, XmlResource.makeAbscentResource());

                i1++;
                i2++;
            }
        }

        if (i1 < list1.size()) {
            for (int i = i1; i < list1.size(); i++) {
                list1.get(i).setStatus(XmlResourceStatusEnum.MATCHED);

                list2.add(XmlResource.makeAbscentResource());
            }
        } else if (i2 < list2.size()) {
            for (int i = i2; i < list2.size(); i++) {
                list1.add(XmlResource.makeAbscentResource());

                list2.get(i).setStatus(XmlResourceStatusEnum.MATCHED);
            }
        }
    }
}
