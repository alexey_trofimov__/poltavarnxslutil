package ru.it.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "XML_RESOURCE_FOLDER")
public class XmlResourceFolder {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FOLDER_NAME")
    private String folderName;

    @OneToMany(mappedBy = "folder", fetch = FetchType.LAZY)
    private List<XmlResource> resources;
}
