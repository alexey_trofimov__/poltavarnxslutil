package ru.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class XmlResourcePair {
    private XmlResource first;
    private XmlResource second;

    public XmlResourceStatusEnum getStatus() {
        if (first.getStatus().equals(XmlResourceStatusEnum.ABSCENT) || second.getStatus().equals(XmlResourceStatusEnum.ABSCENT)) {
            return XmlResourceStatusEnum.ABSCENT;
        }

        if (first.getStatus().equals(XmlResourceStatusEnum.UNMATCHED) || second.getStatus().equals(XmlResourceStatusEnum.UNMATCHED)) {
            return XmlResourceStatusEnum.UNMATCHED;
        }

        return XmlResourceStatusEnum.MATCHED;
    }

    @Override
    public String toString() {
        return String.format("First: %s, status: %s; Second: %s, status %s",
                first.getDocumentName(), first.getStatus().name(), second.getDocumentName(), second.getStatus().name());
    }
}
