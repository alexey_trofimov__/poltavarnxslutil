package ru.it.model;

public enum XmlResourceStatusEnum {
    ABSCENT,
    MATCHED,
    UNMATCHED
}
