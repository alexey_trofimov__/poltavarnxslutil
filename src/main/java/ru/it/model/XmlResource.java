package ru.it.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Objects;

@NoArgsConstructor
@Data
@Entity
@Table(name = "XML_RESOURCE")
public class XmlResource implements Comparable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RESOURCE_ROOT_DOCUMENT")
    private Short resourceRootDocument;

    @Column(name = "URI")
    private String uri;

    @Column(name = "DOCUMENT_NAME")
    private String documentName;

    @Column(name = "CONTENT")
    private String content;


    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "FOLDER_ID", nullable = false)
    private XmlResourceFolder folder;

    @Transient
    private XmlResourceStatusEnum status;

    public XmlResource(Long id, String documentName) {
        this.id = id;
        this.documentName = documentName;
    }

    @Override
    public String toString() {
        return this.getDocumentName();
    }

    public String toStringFullInfo() {
        return String.format("URI=%s, DOCUMENT_NAME=%s, FOLDER_NAME=%s", uri, documentName, folder != null ? folder.getFolderName() : null);
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof XmlResource)) {
            return 1;
        }

        return this.getDocumentName().compareTo(((XmlResource) o).getDocumentName());
    }

    public static XmlResource makeAbscentResource() {
        XmlResource abscent = new XmlResource();
        abscent.setDocumentName("Отсутствует");
        abscent.setStatus(XmlResourceStatusEnum.ABSCENT);
        return abscent;
    }

    public static boolean compareContents(XmlResource doc1, XmlResource doc2) {
        if (Objects.isNull(doc1)
                || Objects.isNull(doc2)
                || Objects.isNull(doc1.getContent())
                || Objects.isNull(doc2.getContent())) {
            return false;
        }

        return doc1.getContent().trim().equals(doc2.getContent().trim());
    }
}
