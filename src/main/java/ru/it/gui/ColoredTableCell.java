package ru.it.gui;

import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourcePair;
import ru.it.model.XmlResourceStatusEnum;

import java.util.Objects;

@Slf4j
public class ColoredTableCell extends TableCell<XmlResourcePair, String> {

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (Objects.isNull(item)) {
            return;
        }

        try {
            setText(item);

            if (getTableRow().getItem() instanceof XmlResourcePair) {
                String columnId = this.getTableColumn().getId();

                XmlResource data = null;

                switch (columnId.toLowerCase()) {
                    case "sourceresourcecolumn":
                        data = ((XmlResourcePair) getTableRow().getItem()).getFirst();

                        // если отстуствуют данные в БД-источнике - то такую строку выбирать для копирования запрещаем
                        if (Objects.nonNull(data)) {
                            this.getTableRow().setDisable(data.getStatus().equals(XmlResourceStatusEnum.ABSCENT));
                        }

                        break;
                    case "targetresourcecolumn":
                        data = ((XmlResourcePair) getTableRow().getItem()).getSecond();
                        break;
                }

                if (Objects.nonNull(data)) {
                    Color color;
                    if (data.getStatus().equals(XmlResourceStatusEnum.ABSCENT)) {
                        color = Color.RED;
                    } else {
                        color = data.getStatus().equals(XmlResourceStatusEnum.UNMATCHED)
                                ? Color.ORANGE
                                : Color.GREEN;
                    }

                    setTextFill(color);
                }
            }
        } catch (Exception e) {
            log.error(null, e);
        }
    }
}
