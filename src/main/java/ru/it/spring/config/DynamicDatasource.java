package ru.it.spring.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDatasource extends AbstractRoutingDataSource{

    private static ThreadLocal<String> schemes = new ThreadLocal<>();

    @Override
    protected Object determineCurrentLookupKey() {
        return schemes.get();
    }

    public static void setScheme(String scheme) {
        schemes.set(scheme);
    }
}
