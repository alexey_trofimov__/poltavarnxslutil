package ru.it.spring.config;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.it.spring.Application;
import ru.it.spring.gui.MainController;
import ru.it.spring.model.DatasourceYAMLConfig;
import ru.it.spring.services.LocalFileSystemHelper;
import ru.it.spring.services.SVNHelper;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

@Slf4j
@Configuration
@ComponentScan(basePackages = "ru.it.spring")
@EnableConfigurationProperties(value = DatasourceYAMLConfig.class)
@EnableTransactionManagement
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ControllersConfiguration {
    private final String PACKAGE_SCAN = "ru.it.model";

    private final SVNHelper svnHelper;
    private final LocalFileSystemHelper localFileSystemHelper;

    @Getter
    private final DatasourceYAMLConfig datasourceYAMLConfig;

    @Bean(name = "mainView")
    public ViewHolder getMainView() throws IOException {
        return loadView("main.fxml");
    }

    @Bean(name = "datasourceConfig")
    public List<DatasourceYAMLConfig.DataSourceConfigElement> getDatasourceYAMLConfig() {
        return datasourceYAMLConfig.getDatasources();
    }

    @Bean
    public MainController getMainController() throws IOException {
        return (MainController) getMainView().getController();
    }

    /**
     * Самый обыкновенный способ использовать FXML загрузчик.
     * Как раз-таки на этом этапе будет создан объект-контроллер,
     * произведены все FXML инъекции и вызван метод инициализации контроллера.
     */
    protected ViewHolder loadView(String url) {
        try (InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream(url)) {
            FXMLLoader loader = new FXMLLoader();
            loader.load(fxmlStream);

            return new ViewHolder(loader.getRoot(), loader.getController());
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    @Bean
    DataSource dataSource(Environment env) {
        DynamicDatasource dataSource = new DynamicDatasource();
        Map<Object, Object> allConfiguredDataSources = new HashMap<>();

        if (Objects.nonNull(datasourceYAMLConfig) || Objects.nonNull(datasourceYAMLConfig.getDatasources())) {
            datasourceYAMLConfig.getDatasources().forEach(k -> {
                switch (k.getDriver().toLowerCase()) {
                    case "svn":
                        svnHelper.addConnectionInfo(k.getName(), k.getUrl(), k.getUsername(), k.getPassword());
                        break;
                    case "file":
                        localFileSystemHelper.addPathInfo(k.getName(), k.getUrl());
                        break;
                    default:
                        DriverManagerDataSource ds = new DriverManagerDataSource(k.getUrl(), k.getUsername(), k.getPassword());
                        ds.setDriverClassName(k.getDriver());

                        allConfiguredDataSources.put(k.getName(), ds);
                }
            });
        }

        if (allConfiguredDataSources.isEmpty()) {
            String driver = "org.apache.derby.jdbc.EmbeddedDriver";
            DriverManagerDataSource defaultTargetDataSource = new DriverManagerDataSource("jdbc:derby:memory:TestingDB1;create=true", "", "");
            defaultTargetDataSource.setDriverClassName(driver);

            dataSource.setDefaultTargetDataSource(defaultTargetDataSource);

            allConfiguredDataSources.put(driver, defaultTargetDataSource);
        }

        dataSource.setTargetDataSources(allConfiguredDataSources);

        return dataSource;
    }

    @Bean(name = "multiEntityManager")
    public LocalContainerEntityManagerFactoryBean multiEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        DataSource ds = dataSource(null);
        if (Objects.nonNull(ds)) {
            em.setDataSource(ds);
        }
        em.setPackagesToScan(PACKAGE_SCAN);

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(hibernateProperties());
        return em;
    }

    @Bean(name = "multiTransactionManager")
    public PlatformTransactionManager multiTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(multiEntityManager().getObject());
        return transactionManager;
    }

//    @Primary
//    @Bean(name = "dbSessionFactory")
//    public LocalSessionFactoryBean dbSessionFactory() {
//        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
//        sessionFactoryBean.setDataSource(dataSource(null));
//        sessionFactoryBean.setPackagesToScan(PACKAGE_SCAN);
//        sessionFactoryBean.setHibernateProperties(hibernateProperties());
//        return sessionFactoryBean;
//    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", true);
        properties.put("hibernate.format_sql", true);
        properties.put("hibernate.dialect", "org.hibernate.dialect.DerbyTenSevenDialect");
        return properties;
    }

    /**
     * Класс - оболочка: контроллер мы обязаны указать в качестве бина,
     * а view - представление, нам предстоит использовать в точке входа {@link Application}.
     */
    @AllArgsConstructor
    @Getter
    @Setter
    public class ViewHolder {
        private Parent view;
        private Object controller;
    }
}
