package ru.it.spring.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.it.spring.config.ApplicationContextHolder;
import ru.it.spring.services.SVNHelper;
import ru.it.spring.services.XmlResourceOperationsIF;
import ru.it.gui.ColoredTableCell;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourceFolder;
import ru.it.model.XmlResourcePair;
import ru.it.model.XmlResourceStatusEnum;
import ru.it.spring.config.ControllersConfiguration;
import ru.it.spring.config.DynamicDatasource;
import ru.it.spring.services.LocalFileSystemHelper;
import ru.it.util.DocumentsListsConformer;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor
@Component
public class MainController implements Initializable {
    @Autowired
    @Qualifier("xmlResourceServiceImpl")
    XmlResourceOperationsIF xmlResourceService;

    @Autowired
    private SVNHelper svnHelper;
    @Autowired
    private LocalFileSystemHelper localFileSystemHelper;

    @FXML
    ComboBox<String> sourcesComboBox;

    @FXML
    ComboBox<String> targetsComboBox;

    @FXML
    ComboBox<String> sourceFoldersComboBox;

    @FXML
    ComboBox<String> targetFoldersComboBox;

    @FXML
    TableView<XmlResourcePair> resourcesTableView;

    @FXML
    TableColumn<XmlResourcePair, String> sourceResourceColumn;

    @FXML
    TableColumn<XmlResourcePair, String> matchesColumn;

    @FXML
    TableColumn<XmlResourcePair, String> targetResourceColumn;

    @FXML
    Button buttonCopy;

    @FXML
    ProgressBar copyProgressBar;

    private List<XmlResource> sourceResources;
    private List<XmlResource> targetResources;

    XmlResourceOperationsIF sourceEndpoint;
    XmlResourceOperationsIF targetEndpoint;

    private String sourceConfigName;
    private String targetConfigName;
    private String sourceFolderName;
    private String targetFolderName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ControllersConfiguration controllersConfiguration = ApplicationContextHolder.getBean(ControllersConfiguration.class);

        List<String> schemeNames = controllersConfiguration.getDatasourceYAMLConfig().stream()
                    .map(ds -> ds.getName())
                    .collect(Collectors.toList());

        sourcesComboBox.setItems(FXCollections.observableArrayList(schemeNames));
        targetsComboBox.setItems(FXCollections.observableArrayList(schemeNames));

        sourceResourceColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirst().getDocumentName()));
        targetResourceColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSecond().getDocumentName()));

        sourceResourceColumn.setCellFactory(c -> new ColoredTableCell());
        targetResourceColumn.setCellFactory(c -> new ColoredTableCell());

        matchesColumn.setCellValueFactory(cellData ->
                new SimpleStringProperty(
                        cellData.getValue().getStatus().equals(XmlResourceStatusEnum.UNMATCHED)
                                ? "=/="
                                : cellData.getValue().getStatus().equals(XmlResourceStatusEnum.ABSCENT)
                                    ? "   "
                                    : "=="
                )
        );

        resourcesTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    public void onSourceSelected() {
        sourceConfigName = sourcesComboBox.getValue();

        sourceFoldersComboBox.getItems().clear();
        sourceFolderName = null;

        if (Objects.nonNull(svnHelper.getConnectionByName(sourceConfigName))) { //имеем дело с SVN
            SVNHelper.SVNConnectionInfo connectionInfo = svnHelper.getConnectionByName(sourceConfigName);

            if (Objects.nonNull(connectionInfo)) {
                try {
                    sourceResources = svnHelper.getXmlResources(sourceConfigName, null);
                    sourceEndpoint = svnHelper;

                    fillTable();
                } catch (Exception e) {
                    Alert a = new Alert(Alert.AlertType.ERROR);

                    log.error("Ошибка соединения с svn-репозиторием", e);

                    a.setContentText("Ошибка соединения с svn-репозиторием: " + e.getMessage());
                    a.show();
                }
            }
        } else if (Objects.nonNull(localFileSystemHelper.getPathByName(sourceConfigName))) { //имеем дело с файловой системой
            sourceResources = localFileSystemHelper.getXmlResources(sourceConfigName, null);
            sourceEndpoint = localFileSystemHelper;

            fillTable();
        } else { //обычное ДБ-соединение
            List<XmlResourceFolder> folders = xmlResourceService.getFolders(sourceConfigName);

            sourceFoldersComboBox.setItems(FXCollections.observableArrayList(
                    folders.stream()
                            .map(folder -> folder.getFolderName())
                            .collect(Collectors.toList())
            ));

            sourceFoldersComboBox.setDisable(false);
            sourceEndpoint = xmlResourceService;

            if (Objects.nonNull(sourceResources)) {
                sourceResources.clear();
            }

            clearTable();
        }
    }

    public void onTargetSelected() {
        targetConfigName = targetsComboBox.getValue();

        targetFoldersComboBox.getItems().clear();
        targetFolderName = null;

        if (Objects.nonNull(svnHelper.getConnectionByName(targetConfigName))) { //имеем дело с SVN
            SVNHelper.SVNConnectionInfo connectionInfo = svnHelper.getConnectionByName(targetConfigName);

            if (Objects.nonNull(connectionInfo)) {
                try {
                    targetResources = svnHelper.getXmlResources(targetConfigName, null);
                    targetEndpoint = svnHelper;

                    fillTable();
                } catch (Exception e) {
                    Alert a = new Alert(Alert.AlertType.ERROR);

                    log.error("Ошибка соединения с svn-репозиторием", e);

                    a.setContentText("Ошибка соединения с svn-репозиторием: " + e.getMessage());
                    a.show();
                }
            }
        } else if (Objects.nonNull(localFileSystemHelper.getPathByName(targetConfigName))) { //имеем дело с файловой системой
            targetResources = localFileSystemHelper.getXmlResources(targetConfigName, null);
            targetEndpoint = localFileSystemHelper;

            fillTable();
        } else { //обычное ДБ-соединение
            List<XmlResourceFolder> folders = xmlResourceService.getFolders(targetConfigName);

            targetFoldersComboBox.setItems(FXCollections.observableArrayList(folders.stream()
                    .map(folder -> folder.getFolderName())
                    .collect(Collectors.toList())));

            targetFoldersComboBox.setDisable(false);
            targetEndpoint = xmlResourceService;

            if (Objects.nonNull(targetResources)) {
                targetResources.clear();
            }

            clearTable();
        }
    }

    public void onSourceFolderSelected() {
        sourceFolderName = sourceFoldersComboBox.getValue();
        sourceResources = Optional.ofNullable(sourceFolderName)
            .map(fn -> sourceEndpoint.getXmlResources(sourceConfigName, sourceFolderName))
                .orElse(new ArrayList<>());

        fillTable();
    }

    public void onTargetFolderSelected() {
        targetFolderName = targetFoldersComboBox.getValue();

        targetResources = Optional.ofNullable(targetFolderName)
            .map(fn -> targetEndpoint.getXmlResources(targetConfigName, targetFolderName))
                .orElse(new ArrayList<>());

        fillTable();
    }

    private void fillTable() {
        if (Objects.isNull(targetResources) || Objects.isNull(sourceResources)) { //ничего не делаем
            return;
        }

        try {
            DocumentsListsConformer.conformLists(sourceResources, targetResources);

            List<XmlResourcePair> pairs = new ArrayList<>();

            for (int i = 0; i < sourceResources.size(); i++) {
                pairs.add(new XmlResourcePair(sourceResources.get(i), targetResources.get(i)));
            }

            resourcesTableView.setDisable(false);
            resourcesTableView.setItems(FXCollections.observableArrayList(pairs));
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    private void clearTable() {
        resourcesTableView.getItems().clear();
    }

    public void copyResources() {
        log.info("Copying resources...");

        copyProgressBar.setProgress(0);

        List<XmlResourcePair> resourcesToCopy = resourcesTableView.getSelectionModel().getSelectedItems();

        resourcesToCopy.forEach(r -> {
            // выставляем ресурсу xml-папку приемника
            r.getFirst().setFolder(new XmlResourceFolder());
            r.getFirst().getFolder().setFolderName(targetFoldersComboBox.getValue());

            if (r.getSecond().getStatus().equals(XmlResourceStatusEnum.ABSCENT)) {
                DynamicDatasource.setScheme(targetConfigName);
                targetEndpoint.addXmlResource(targetConfigName, r.getFirst());
            } else {
                targetEndpoint.modifyXmlResource(targetConfigName, r.getSecond(), r.getFirst());
            }

            copyProgressBar.setProgress(copyProgressBar.getProgress() + (1 / resourcesToCopy.size()));
        });

        onTargetFolderSelected();
    }
}
