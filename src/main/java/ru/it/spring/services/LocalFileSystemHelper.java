package ru.it.spring.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.it.util.StringUtil;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourceFolder;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

@Slf4j
@Component("localFileSystemHelper")
public class LocalFileSystemHelper implements XmlResourceOperationsIF {
    private static Map<String, Path> paths = new HashMap<>();;

    @Override
    public List<XmlResourceFolder> getFolders(String configName) {
        throw new NotImplementedException();
    }

    @Override
    public List<XmlResource> getXmlResources(String configName, String folderName) {
        List<XmlResource> result = new ArrayList<>();

        try {
            if (!paths.containsKey(configName)) {
                throw new RuntimeException("No path info found!");
            }

            result = Files.list(paths.get(configName))
                    .filter(entry -> entry.getFileName().toString().matches(".*\\.x((ml)|(sl)|(sd))"))
                    .map(entry -> {
                        XmlResource xmlResource = new XmlResource();
                        xmlResource.setDocumentName(entry.getFileName().toString());
                        xmlResource.setId(-1L);
                        xmlResource.setResourceRootDocument((short) 0);
                        try {
                            xmlResource.setContent(new String(Files.readAllBytes(entry), StandardCharsets.UTF_8));

                            if (xmlResource.getDocumentName().endsWith(".xsd")) {
                                xmlResource.setUri(StringUtil.defineXsdTargetNamespace(xmlResource.getContent()));
                            }

                            return xmlResource;
                        } catch (Exception e) {
                            log.warn(String.format("Failed reading content for file %s", entry.getFileName()), e);
                            return null;
                        }
                    })
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(String.format("Failed to obtain file list from folder %s", configName), e);
        }
        return result;
    }

    @Override
    public void addXmlResource(String configName, XmlResource xml) {
        if (!paths.containsKey(configName)) {
            throw new RuntimeException("Failed to save file: no path info found!");
        }

        Path path = paths.get(configName).resolve(xml.getDocumentName());

        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(path, CREATE, APPEND))) {
            byte[] data = xml.getContent().getBytes("UTF-8");
            out.write(data, 0, data.length);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to save file into folder %s", configName), e);
        }
    }

    @Override
    public void deleteXmlResource(String configName, XmlResource xml) {
        if (!paths.containsKey(configName)) {
            throw new RuntimeException("Failed to delete file: no path info found!");
        }

        Path path = paths.get(configName).resolve(xml.getDocumentName());
        path.toFile().deleteOnExit();
    }

    @Override
    public void modifyXmlResource(String configName, XmlResource oldOne, XmlResource newOne) {
        if (!paths.containsKey(configName)) {
            throw new RuntimeException("Failed to delete file: no path info found!");
        }

        deleteXmlResource(configName, oldOne);
        addXmlResource(configName, newOne);
    }

    public void addPathInfo(String name, String _path) {
        boolean error = false;
        Path path = null;

        try {
            path = Paths.get(_path);
        } catch (Exception e) {
            error = true;
        }

        if (!Files.exists(path) || !Files.isDirectory(path)) {
            error = true;
        }

        if (error) {
            log.error("Неверный путь в файловой системе: {} (не существует либо не является каталогом)", path);
        } else {
            paths.put(name, path);
        }
    }

    public Path getPathByName(String name) {
        return paths.get(name);
    }
}
