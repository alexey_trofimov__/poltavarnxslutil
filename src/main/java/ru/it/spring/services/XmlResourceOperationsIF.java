package ru.it.spring.services;

import ru.it.model.XmlResource;
import ru.it.model.XmlResourceFolder;

import java.util.List;

public interface XmlResourceOperationsIF {
    List<XmlResourceFolder> getFolders(String configName);
    List<XmlResource> getXmlResources(String configName, String folderName);
    void addXmlResource(String configName, XmlResource xml);
    void deleteXmlResource(String configName, XmlResource xml);
    void modifyXmlResource(String configNamer, XmlResource oldOne, XmlResource newOne);
}
