package ru.it.spring.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.BasicAuthenticationManager;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import ru.it.util.StringUtil;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourceFolder;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component("svnHelper")
public class SVNHelper implements XmlResourceOperationsIF {

    private static Map<String, SVNConnectionInfo> connectionInfos;

    private SVNRepository repository;

    public static void addConnectionInfo(String name, String url, String username, String password) {
        if (Objects.isNull(connectionInfos)) {
            SVNRepositoryFactoryImpl.setup();
            connectionInfos = new HashMap<>();
        }

        connectionInfos.put(name, new SVNConnectionInfo(url, username, password));
    }

    public SVNConnectionInfo getConnectionByName(String name) {
        return Objects.isNull(connectionInfos) ? null : connectionInfos.get(name);
    }

    @Override
    public List<XmlResourceFolder> getFolders(String configName) {
        throw new NotImplementedException();
    }

    @Override
    public List<XmlResource> getXmlResources(String configName, String folderName) {
        try {
            SVNConnectionInfo svnConnectionInfo = getConnectionByName(configName);

            repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(svnConnectionInfo.getUrl()));
            repository.setAuthenticationManager(SVNWCUtil.createDefaultAuthenticationManager(svnConnectionInfo.getUsername(), svnConnectionInfo.getPassword()));

            if (Objects.isNull(repository)) {
                log.error("Отсутствует информация об svn-соединении");
            } else {
                return readResources(repository, "xml", "xsd", "xsl");
            }
        } catch (Exception e) {
            log.error(null, e);
        }

        return null;
    }

    @Override
    public void addXmlResource(String configName, XmlResource xml) {
        try {
            addOrModifyFile(xml.getDocumentName(), xml.getContent().getBytes(StandardCharsets.UTF_8), true);
        } catch (Exception e) {
            log.error("Не удалось создать файл {}", xml.getDocumentName(), e);
        }
    }

    @Override
    public void deleteXmlResource(String configName, XmlResource xml) {
        throw new NotImplementedException();
    }

    @Override
    public void modifyXmlResource(String configName, XmlResource oldOne, XmlResource newOne) {
        try {
            addOrModifyFile(oldOne.getDocumentName(), newOne.getContent().getBytes(StandardCharsets.UTF_8), false);
        } catch (Exception e) {
            log.error("Не удалось обновит файл {}", oldOne.getDocumentName(), e);
        }
    }

    private List<XmlResource> readResources(SVNRepository repository, String... extensions) throws Exception {
        Collection<SVNDirEntry> entries = repository.getDir( "", -1 , null , (Collection) null );
        List<XmlResource> result = new ArrayList<>();

        for (SVNDirEntry entry : entries) {
            String fileName = entry.getName();

            Arrays.stream(extensions)
                    .forEach(e -> {
                        if (fileName.endsWith("." + e)) {
                            XmlResource xmlResource = new XmlResource();
                            xmlResource.setId(-1L);
                            xmlResource.setDocumentName(fileName);
                            SVNProperties fileProperties = new SVNProperties();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream( );

                            try {
                                repository.getFile(fileName, -1, fileProperties, baos);
                                xmlResource.setContent(new String(baos.toByteArray(), StandardCharsets.UTF_8));

                                if (xmlResource.getDocumentName().endsWith(".xsd")) {
                                    xmlResource.setUri(StringUtil.defineXsdTargetNamespace(xmlResource.getContent()));
                                }

                                result.add(xmlResource);
                            } catch (Exception e1) {
                                log.error("Не удалось прочитать данные файла {}", fileName, e1);
                            }
                        }
                    });

            log.debug(null, entry);
        }

        return result;
    }

    public SVNRepository makeRepository(SVNConnectionInfo connectionInfo) throws Exception {
        SVNURL url = SVNURL.parseURIEncoded(connectionInfo.url);
        SVNRepository repository = SVNRepositoryFactory.create( url, null );
        ISVNAuthenticationManager authManager = new BasicAuthenticationManager(connectionInfo.username , connectionInfo.password);

        repository.setAuthenticationManager(authManager);

        //проверяем, что путь указывает на каталог, а не на файл
        SVNNodeKind nodeKind = repository.checkPath( "" ,  -1 );

        if (nodeKind == SVNNodeKind.NONE) {
            throw new Exception("Указан неверный путь к каталогу: " + connectionInfo.url);
        } else if (nodeKind == SVNNodeKind.FILE) {
            throw new Exception("Путь " + connectionInfo.url + " некорректен: указывает на файл, а не на каталог");
        }

        return repository;
    }

    /**
     * Методология добавления и обновленмя файла взята из этого примера:
     * https://svn.svnkit.com/repos/svnkit/tags/1.1.1/doc/examples/src/org/tmatesoft/svn/examples/repository/Commit.java
     * @param fileName
     * @param data
     * @throws Exception
     */
    private void addOrModifyFile(String fileName, byte[] data, boolean isNew) throws Exception {
        ISVNEditor editor = repository.getCommitEditor("commit message", null);
        editor.openRoot(-1);

        if (isNew) {
            editor.addFile(fileName, null, -1);
        } else {
            editor.openFile(fileName, -1);
        }

        editor.applyTextDelta(fileName, null);
        SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
        String checksum = deltaGenerator.sendDelta(fileName, new ByteArrayInputStream(data), editor, true);

        editor.closeFile(fileName, checksum);
        editor.closeDir();
        editor.closeEdit();
    }

    @AllArgsConstructor
    @Getter
    public static class SVNConnectionInfo {
        String url;
        String username;
        String password;
    }
}
