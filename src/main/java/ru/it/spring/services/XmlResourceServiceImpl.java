package ru.it.spring.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it.model.XmlResource;
import ru.it.model.XmlResourceFolder;
import ru.it.spring.config.DynamicDatasource;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service("xmlResourceServiceImpl")
public class XmlResourceServiceImpl implements XmlResourceOperationsIF {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<XmlResourceFolder> getFolders(String configName) {
        // Делать это надо обязательно перед началом транзакции!!
        // Иначе сработает уже после транзакции, а транзакция отработает с дефолтовым датасорсом
        DynamicDatasource.setScheme(configName);
        List<XmlResourceFolder> result = getFolders();

        log.info("Got {} folders for config {}", result.size(), configName);

        return result;
    }

    @Transactional(readOnly = true)
    public List<XmlResourceFolder> getFolders() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<XmlResourceFolder> q = cb.createQuery(XmlResourceFolder.class);
        Root<XmlResourceFolder> root = q.from(XmlResourceFolder.class);

        TypedQuery<XmlResourceFolder> foldersQuery = entityManager.createQuery(q.select(root).orderBy(cb.asc(root.get("folderName"))));

        return foldersQuery.getResultList();
    }

    @Override
    public List<XmlResource> getXmlResources(String configName, String folderName) {
        log.info("getXmlResources(\"{}, {}\")", configName, folderName);

        // Делать это надо обязательно перед началом транзакции!!
        // Иначе сработает уже после транзакции, а транзакция отработает с дефолтовым датасорсом
        DynamicDatasource.setScheme(configName);
        List<XmlResource> result = getXmlResources(folderName);

        log.info("Got {} xml resources for config {}, folder {}", result.size(), configName, folderName);

        return result;
    }

    @Transactional(readOnly = true)
    public List<XmlResource> getXmlResources(String folderName) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<XmlResource> criteriaQuery = cb.createQuery(XmlResource.class);

        Root<XmlResource> q = criteriaQuery.from(XmlResource.class);
        criteriaQuery.select(q).where(cb.equal(q.get("folder").get("folderName"), folderName));

        TypedQuery<XmlResource> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void addXmlResource(String configName, XmlResource xml) {
        // А здесь делать это надо, ебанаврот, обязательно ПОСЛЕ начала транзакции!!
        // Иначе вообще не сработает :((((
        // Ключевой момент, бля, readonly = true есть или нет...

        DynamicDatasource.setScheme(configName);
        addXmlResource(xml);
    }

    @Transactional
    public void addXmlResource(XmlResource xml) {
        log.info("addXmlResource(\"{}\")", xml.getDocumentName());

        //подгружаем папку по наименованию
        if (Objects.nonNull(xml.getFolder()) && !StringUtils.isBlank(xml.getFolder().getFolderName())) {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();

            CriteriaQuery<XmlResourceFolder> criteriaQuery = cb.createQuery(XmlResourceFolder.class);
            Root<XmlResourceFolder> q = criteriaQuery.from(XmlResourceFolder.class);
            criteriaQuery.select(q).where(cb.equal(q.get("folderName"), xml.getFolder().getFolderName()));

            XmlResourceFolder result = entityManager.createQuery(criteriaQuery).getSingleResult();

            if (Objects.nonNull(result)) {
                xml.setFolder(result);
            }
        }

        entityManager.merge(xml);
    }

    @Override
    @Transactional
    public void deleteXmlResource(String configName, XmlResource xml) {
        // А здесь делать это надо, ебанаврот, обязательно ПОСЛЕ начала транзакции!!
        // Иначе вообще не сработает :((((
        // Ключевой момент, бля, readonly = true есть или нет...
        DynamicDatasource.setScheme(configName);

        deleteXmlResource(xml);
    }

    @Transactional
    public void deleteXmlResource(XmlResource xml) {
        log.debug("deleteXmlResource(\"{}\")", xml.getDocumentName());

        if (!entityManager.contains(xml)) { //возможно, сущность будет detached
            //entityManager.merge(xml);
            xml = entityManager.find(XmlResource.class, xml.getId());
        }

        entityManager.remove(xml);
    }

    @Override
    public void modifyXmlResource(String configName, XmlResource oldOne, XmlResource newOne) {
        // Делать это надо обязательно перед началом транзакции!!
        // Иначе сработает уже после транзакции, а транзакция отработает с дефолтовым датасорсом
        DynamicDatasource.setScheme(configName);

        modifyXmlResource(oldOne, newOne);
    }

    @Transactional
    public void modifyXmlResource(XmlResource oldOne, XmlResource newOne) {
        deleteXmlResource(oldOne);
        addXmlResource(newOne);
    }
}
