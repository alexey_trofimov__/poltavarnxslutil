package ru.it.spring.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@Configuration
@ConfigurationProperties(prefix = "config")
@EnableConfigurationProperties
public class DatasourceYAMLConfig {

    List<DataSourceConfigElement> datasources;

    @Getter
    @Setter
    @ToString
    public static class DataSourceConfigElement {
        String name;
        String driver;
        String url;
        String username;
        String password;
    }
}
