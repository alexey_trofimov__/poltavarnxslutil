//package config;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.*;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
////import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import ru.it.wutsky.spring.config.DynamicDatasource;
//import ru.it.wutsky.spring.model.DatasourceYAMLConfig;
//import ru.it.wutsky.spring.services.LocalFileSystemHelper;
//import ru.it.wutsky.spring.services.SVNHelper;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Objects;
//import java.util.Properties;
//
///**
// *
// * @author atrofimov
// */
//@Configuration
//@ComponentScan(basePackages = "ru.it.wutsky.spring")
//@EnableTransactionManagement
//
////это необязательно, если не используем JpaRepository
//@EnableJpaRepositories(
//        basePackages = "ru.it.wutsky.model",
//        entityManagerFactoryRef = "multiEntityManager",
//        transactionManagerRef = "multiTransactionManager"
//)
//
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//@Profile("test")
//public class SpringTestConfiguration {
//    private final String PACKAGE_SCAN = "ru.it.wutsky.model";
//
//    private final SVNHelper svnHelper;
//    private final LocalFileSystemHelper localFileSystemHelper;
//    private final DatasourceYAMLConfig datasourceYAMLConfig;
//
//    //вариативный датасорс - может указывать как на исходную базу, так и на целевую
//    @Bean
//    DataSource dataSource(Environment env) {
//        DynamicDatasource dataSource = new DynamicDatasource();
//
//        if (Objects.isNull(datasourceYAMLConfig) || Objects.isNull(datasourceYAMLConfig.getDatasources())) {
//            return dataSource;
//        }
//
//        Map<Object, Object> allConfiguredDataSources = new HashMap<>();
//
//        datasourceYAMLConfig.getDatasources().forEach(k -> {
//            switch (k.getDriver().toLowerCase()) {
//                case "svn":
//                    svnHelper.addConnectionInfo(k.getName(), k.getUrl(), k.getUsername(), k.getPassword());
//                    break;
//                case "file":
//                    localFileSystemHelper.addPathInfo(k.getName(), k.getUrl());
//                    break;
//                default:
//                    DriverManagerDataSource ds = new DriverManagerDataSource();
//                    ds.setDriverClassName(k.getDriver());
//                    ds.setUrl(k.getUrl());
//                    ds.setUsername(k.getUsername());
//                    ds.setPassword(k.getPassword());
//
//                    allConfiguredDataSources.put(k.getName(), ds);
//            }
//        });
//
//        if (!allConfiguredDataSources.isEmpty()) {
//            dataSource.setTargetDataSources(allConfiguredDataSources);
//            dataSource.setDefaultTargetDataSource(allConfiguredDataSources.get("DERBY1"));
//        }
//
//        return dataSource;
//    }
//
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(dataSource(null));
//        em.setPackagesToScan(PACKAGE_SCAN);
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        em.setJpaVendorAdapter(vendorAdapter);
//        em.setJpaProperties(hibernateProperties());
//        return em;
//    }
//
//    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(entityManagerFactory);
//        return transactionManager;
//    }
//
////    @Primary
////    @Bean(name = "dbSessionFactory")
////    public LocalSessionFactoryBean dbSessionFactory() {
////        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
////        sessionFactoryBean.setDataSource(dataSource(null));
////        sessionFactoryBean.setPackagesToScan(PACKAGE_SCAN);
////        sessionFactoryBean.setHibernateProperties(hibernateProperties());
////        return sessionFactoryBean;
////    }
//
//    private Properties hibernateProperties() {
//        Properties properties = new Properties();
//        properties.put("hibernate.show_sql", true);
//        properties.put("hibernate.format_sql", true);
//
//        //Configures the used database dialect. This allows Hibernate to create SQL
//        //that is optimized for the used database.
//        //properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
//
//        //Specifies the action that is invoked to the database when the Hibernate
//        //SessionFactory is created or closed.
//        properties.put("hibernate.hbm2ddl.auto", "create-drop");
//
//        //Configures the naming strategy that is used when Hibernate creates
//        //new database objects and schema elements
//        properties.put("hibernate.ejb.naming_strategy","org.hibernate.cfg.ImprovedNamingStrategy");
//        return properties;
//    }
//}
