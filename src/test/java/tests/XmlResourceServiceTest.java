//package tests;
//
//import config.SpringTestConfiguration;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.jdbc.Sql;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import ru.it.wutsky.model.DBConfig;
//import ru.it.wutsky.model.XmlResource;
//import ru.it.wutsky.model.XmlResourceFolder;
//import ru.it.wutsky.model.XmlResourceStatusEnum;
//import ru.it.wutsky.spring.config.DynamicDatasource;
//import ru.it.wutsky.spring.services.XmlResourceOperationsIF;
//import ru.it.wutsky.util.DocumentsListsConformer;
//import ru.it.wutsky.util.StringUtil;
//
//import java.util.LinkedList;
//import java.util.List;
//
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
////для теста можно указывать спец. тестовый  контекст, а можно, если угодно,  и основной
//@ContextConfiguration(classes={SpringTestConfiguration.class})
//@ActiveProfiles("test")
//public class XmlResourceServiceTest {
//
//    @Autowired
//    @Qualifier("xmlResourceServiceImpl")
//    XmlResourceOperationsIF xmlResourceService;
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db_test.sql")
//    public void testDBLayer() {
//        log.info("Testing persistence layer");
//
//        DBConfig dbConfig = DBConfig.getInstance("databases.yml");
//        DynamicDatasource.SOURCE_SCHEME_CHOSEN_BY_USER = "DERBY1";
//        DynamicDatasource.TARGET_SCHEME_CHOSEN_BY_USER = "DERBY2";
//
//        List<XmlResource> xmls = xmlResourceService.getXmlResources("DERBY1", "folder_gisgmp_2.1");
//
//        log.info("Xmls found in source DB: {}", xmls.size());
//
//        if (xmls == null || xmls.isEmpty()) {
//            //"обманем" реализацию сервиса - подсунем ему датасорс-источник вместо приемника
//            String target = DynamicDatasource.TARGET_SCHEME_CHOSEN_BY_USER;
//            DynamicDatasource.TARGET_SCHEME_CHOSEN_BY_USER = DynamicDatasource.SOURCE_SCHEME_CHOSEN_BY_USER;
//
//            XmlResourceFolder folder = new XmlResourceFolder();
//            folder.setId(1l);
//            folder.setFolderName("folder_gisgmp_2.1");
//
//            XmlResource resource = new XmlResource();
//            resource.setContent("<crap/>");
//            resource.setDocumentName("Resource.xsl");
//            resource.setId(1l);
//            resource.setResourceRootDocument((short)0);
//            resource.setFolder(folder);
//
//            log.info("Adding resource and folder...");
//
//            xmlResourceService.addXmlResource("DERBY1", resource);
//
//            log.info("Resource 1 and folder saved");
//
//            resource = new XmlResource();
//            resource.setContent("<crap2/>");
//            resource.setDocumentName("Resource2.xsl");
//            resource.setId(2l);
//            resource.setResourceRootDocument((short)0);
//            resource.setFolder(folder);
//
//            log.info("Adding resource2 with existing...");
//
//            xmlResourceService.addXmlResource("DERBY1", resource);
//
//            log.info("Resource 2 saved");
//
//            DynamicDatasource.TARGET_SCHEME_CHOSEN_BY_USER = target;
//        }
//
//        for (XmlResource xml : xmls) {
//            String xslName = "SmartProxyExportChargesResponse.xsl";
//
//            if (xml.getDocumentName().equalsIgnoreCase(xslName)) {
//                log.info("{} found", xslName);
//
//                xmlResourceService.deleteXmlResource("DERBY1", xml);
//
//                log.info("Deleted {} from target DB", xslName);
//
//                xmlResourceService.addXmlResource("DERBY1", xml);
//
//                log.info("Replaced {} in target DB", xslName);
//            }
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testListsConformation() {
//        List<XmlResource> list1 = new LinkedList<>();
//        List<XmlResource> list2 = new LinkedList<>();
//
//        list1.add(new XmlResource(1l, "bbb"));
//        list1.add(new XmlResource(2l, "aaa"));
//        list2.add(new XmlResource(3l, "ccc"));
//        list2.add(new XmlResource(4l, "bbb"));
//        list2.add(new XmlResource(5l, "ddd"));
//
//        try {
//            DocumentsListsConformer.conformLists(list1, list2);
//        } catch (Exception e) {
//            log.error(null, e);
//
//            fail(e.getMessage());
//        }
//
//        assertTrue(list1.size() == 4 && list2.size() == 4);
//
//        for (int i = 0; i < 4; i++) {
//            log.info("{}        {}", list1.get(i).getDocumentName(), list2.get(i).getDocumentName());
//        }
//
//        assertTrue(list1.get(0).getDocumentName().equals("aaa"));
//        assertTrue(list1.get(1).getDocumentName().equals("bbb"));
//        assertTrue(list1.get(2).getStatus() == XmlResourceStatusEnum.ABSCENT);
//        assertTrue(list1.get(3).getStatus() == XmlResourceStatusEnum.ABSCENT);
//
//        assertTrue(list2.get(0).getStatus() == XmlResourceStatusEnum.ABSCENT);
//        assertTrue(list2.get(1).getDocumentName().equals("bbb"));
//        assertTrue(list2.get(2).getDocumentName().equals("ccc"));
//        assertTrue(list2.get(3).getDocumentName().equals("ddd"));
//    }
//
//    @Test
//    @Ignore
//    public void testStringUtil() {
//        String str1 = "12345";
//        String str2 = "1234567890";
//        String str3 = "1234567890abc";
//
//        assertTrue(StringUtil.truncateOrPadStringToFixedLength(str1, 10, ' ').equals("12345     "));
//        assertTrue(StringUtil.truncateOrPadStringToFixedLength(str2, 10, ' ').equals(str2));
//        assertTrue(StringUtil.truncateOrPadStringToFixedLength(str3, 10, ' ').equals(str2));
//    }
//}
