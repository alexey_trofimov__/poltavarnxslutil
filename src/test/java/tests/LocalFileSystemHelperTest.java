//package tests;
//
//import config.SpringTestConfiguration;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import ru.it.wutsky.spring.services.XmlResourceOperationsIF;
//
//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
////для теста можно указывать спец. тестовый  контекст, а можно, если угодно,  и основной
//@ContextConfiguration(classes={SpringTestConfiguration.class})
//@ActiveProfiles("test")
//public class LocalFileSystemHelperTest {
//    @Autowired
//    @Qualifier("localFileSystemHelper")
//    XmlResourceOperationsIF localFileSystemHelper;
//
//    @Test
//    @Ignore
//    public void testListingFiles() {
//        //List<XmlResource> files = localFileSystemHelper.getXmlResources("C:/TEMP");
//        //files.forEach(entry -> log.info(entry.getDocumentName()));
//    }
//}
